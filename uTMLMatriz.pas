unit uTMLMatriz;

interface

uses
  System.SysUtils, System.Classes, contnrs, inifiles, Vcl.Grids,
  uTElementOfMatriz, uTypes, Vcl.Dialogs;

type

  TMLMatriz = class(TComponent)
  private
    FMatriz: TStringGrid;

  protected
    { Protected declarations }
  public
    constructor create;
    destructor destroy;

    procedure LoadFromFile(fileName: string);
    procedure Add(i, j: integer; Value: real);
    procedure PrintMatriz(sg: TStringGrid);

    function get(i, j: integer): real;

    function getDimension: TDimension;
  published

  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('MathLab', [TMLMatriz]);
end;

{ TMLMatriz }

procedure TMLMatriz.Add(i, j: integer; Value: real);
begin
  if i > getDimension.m then
    FMatriz.ColCount := FMatriz.ColCount + 1;

  if j > getDimension.n then
    FMatriz.RowCount := FMatriz.RowCount + 1;

  ShowMessage(IntToStr(FMatriz.ColCount) + 'x' + IntToStr(FMatriz.RowCount));
  FMatriz.Cells[i - 1, j - 1] := FloatToStr(Value);
end;

constructor TMLMatriz.create;
begin
  FMatriz := TStringGrid.create(nil);
  FMatriz.ColCount := 1;
  FMatriz.RowCount := 1;
end;

destructor TMLMatriz.destroy;
begin

end;

function TMLMatriz.get(i, j: integer): real;
begin
  if (i <= getDimension.m) and (j <= getDimension.n) then
  begin
    Result := StrToFloat(FMatriz.Cells[i - 1, j - 1]);
  end;
end;

function TMLMatriz.getDimension: TDimension;
begin
  Result.m := FMatriz.ColCount;
  Result.n := FMatriz.RowCount;
end;

procedure TMLMatriz.LoadFromFile(fileName: string);
var
  matrizFile: TIniFile;
  m, n, i, j: integer;
  Value: real;
begin
  matrizFile := TIniFile.create(fileName);
  FMatriz.ColCount := 1;
  FMatriz.RowCount := 1;

  m := matrizFile.ReadInteger('DIMESION', 'M', 0);
  n := matrizFile.ReadInteger('DIMESION', 'N', 0);

  for i := 1 to m do
  begin
    for j := 1 to n do
    begin
      Value := matrizFile.ReadFloat('Valores', 'val[' + IntToStr(i) + ',' +
        IntToStr(j) + ']', 0);
      Add(i, j, Value);
    end;
  end;
end;

procedure TMLMatriz.PrintMatriz(sg: TStringGrid);
var
  i: integer;
  j: integer;
begin
  sg.RowCount := getDimension.n;
  sg.ColCount := getDimension.m;

  for i := 1 to getDimension.m do
  begin
    for j := 1 to getDimension.n do
    begin
      sg.Cells[i - 1, j - 1] := FMatriz.Cells[i - 1, j - 1];
    end;
  end;

end;

end.
