unit uTElementOfMatriz;

interface

uses Classes, uTypes;

type

  TElementOfMatriz = class(TObject)
  private
    Fvalue: TElement;
    procedure setvalue(const Value: TElement);
  public
    constructor create;
    destructor destroy;
  published
    property element: TElement read Fvalue write setvalue;
  end;

implementation

{ TElementOfMatriz }

constructor TElementOfMatriz.create;
begin

end;

destructor TElementOfMatriz.destroy;
begin

end;

procedure TElementOfMatriz.setvalue(const Value: TElement);
begin
  Fvalue := Value;
end;

end.
