unit uTypes;

interface

uses classes;

type

  TElement = record
    i, j: integer;
    value: real;
  end;

  TDimension = record
    m, n: integer;
  end;

implementation

end.
