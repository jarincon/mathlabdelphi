unit uFExample;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, uTMLOperation, uTMLMatriz,
  Vcl.StdCtrls, Vcl.Grids;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  MLMatriz1: TMLMatriz;
begin
  MLMatriz1 := TMLMatriz.create;

  MLMatriz1.Add(1, 1, 1);
  MLMatriz1.Add(1, 2, 2);
  MLMatriz1.Add(1, 3, 3);
  MLMatriz1.Add(2, 1, 4);
  MLMatriz1.Add(2, 2, 5);
  MLMatriz1.Add(2, 3, 6);
  MLMatriz1.Add(3, 1, 7);
  MLMatriz1.Add(3, 2, 8);
  MLMatriz1.Add(3, 3, 9);

  MLMatriz1.PrintMatriz(StringGrid1);
end;

end.
