program Example;

uses
  Vcl.Forms,
  uFExample in 'uFExample.pas' {Form1},
  uTMLMatriz in '..\uTMLMatriz.pas',
  uTMLOperation in '..\uTMLOperation.pas',
  uTElementOfMatriz in '..\uTElementOfMatriz.pas',
  uTypes in '..\uTypes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
